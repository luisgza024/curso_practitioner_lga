package mis.cursos.springproductos_BACK01;

import mis.cursos.springproductos_BACK01.modelo.Producto;
import mis.cursos.springproductos_BACK01.modelo.Proveedor;
import mis.cursos.springproductos_BACK01.modelo.Usuario;
import mis.cursos.springproductos_BACK01.servicio.ServicioGenerico;
import mis.cursos.springproductos_BACK01.servicio.impl.ServicioGenericoImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringProductosBack01Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringProductosBack01Application.class, args);
	}

	//@Bean
	//ForwardedHeaderFilter forwardedHeaderFilter() {
	//	return new ForwardedHeaderFilter();
	//}

	@Bean
	ServicioGenerico<Producto> servicioProducto() {
		return new ServicioGenericoImpl<Producto>();


	}

	@Bean
	ServicioGenerico<Proveedor> servicioProveedor() {
		return new ServicioGenericoImpl<Proveedor>();
	}

	@Bean
	ServicioGenerico<Usuario> servicioUsuario() {
		return new ServicioGenericoImpl<Usuario>();
	}
}

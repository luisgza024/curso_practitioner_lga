package mis.cursos.springproductos_BACK01.controlador;

import mis.cursos.springproductos_BACK01.modelo.Producto;
import mis.cursos.springproductos_BACK01.modelo.ProductoPrecio;
import mis.cursos.springproductos_BACK01.servicio.ServicioGenerico;
import mis.cursos.springproductos_BACK01.servicio.impl.ServicioGenericoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/almacen/v1/productos")
@ExposesResourceFor(Producto.class)
public class ControladorProducto {
    @Autowired
    ServicioGenerico<Producto> servicioProducto; // Inyección de Dependencias

    @Autowired
    EntityLinks entityLinks;

    @GetMapping
    public CollectionModel<EntityModel<Producto>> obtenerProductos() {

        // EntityModel<T> {..}
        // CollectionModel<T> [{},...]
        // Link { url, rel }
        final Map<Long, Producto> mp = this.servicioProducto.obtenerTodos();
        List<Producto> pp = mp.values().stream().collect(Collectors.toList());

        return CollectionModel.of(
                pp.stream().map(p -> obtenerRespuestaProducto(p)).collect(Collectors.toUnmodifiableList())
        ).add(linkTo(methodOn(this.getClass()).obtenerProductos()).withSelfRel());
    }

    @GetMapping("/{idProducto}")
    public EntityModel<Producto> obtenerProductoPorId(@PathVariable(name = "idProducto") long id) {
        try {
            final Producto p = this.servicioProducto.obtenerPorId(id);
            return obtenerRespuestaProducto(p);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    // nueva linea
    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        Producto pr = servicioProducto.obtenerPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }
// nueva linea


    // nueva linea
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        Producto pr = servicioProducto.obtenerPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    // nueva linea



    @PostMapping
    public ResponseEntity<EntityModel<Producto>> crearProducto(@RequestBody Producto p) {
        // Location: http://localhost:9999/almacen/v1/productos/1
        final long id = this.servicioProducto.agregar(p);
        p.setId(id);
        return ResponseEntity
                .ok()
                .location(crearEnlaceProducto(p).toUri())
                .body(obtenerRespuestaProducto(p))
                ;
    }
// nueva linea
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody Producto producto) {
        servicioProducto.agregar(producto);
    return new ResponseEntity<>("Se creo el producto satisfactoriamente!", HttpStatus.CREATED);
}
// nueva linea

    @PutMapping("/{idProducto}")
    public void reemplazarProductoPorId(@PathVariable(name = "idProducto") long id,
                                        @RequestBody Producto p) {
        try {
            p.setId(id);
            this.servicioProducto.reemplazarPorId(id, p);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    // nueva linea
    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody Producto productToUpdate) {
        Producto pr = servicioProducto.obtenerPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioProducto.reemplazarPorId(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }
    // nueva linea

    static class ParcheProducto {
        public Double precio;
        public Double cantidad;
    }

    @PatchMapping("/{idProducto}")
    public void emparcharProductoPorId(@PathVariable(name = "idProducto") long id,
                                       @RequestBody ParcheProducto p) {
        try {

            final Producto o = this.servicioProducto.obtenerPorId(id);
            if (p.cantidad != null)
                o.setCantidad(p.cantidad);
            if (p.precio != null)
                o.setPrecio(p.precio);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }



    // nueva linea
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecio productoPrecioOnly, @PathVariable int id){
        Producto pr = servicioProducto.obtenerPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        servicioProducto.reemplazarPorId(id, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }
    // nueva linea

    @DeleteMapping("/{idProducto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProductoPorId(@PathVariable(name = "idProducto") long id) {
        this.servicioProducto.borrarPorId(id);
    }
    // nueva linea
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        Producto pr = servicioProducto.obtenerPorId(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioProducto.borrarPorId(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    // nueva linea

    private Link crearEnlaceProducto(Producto p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.getId())
                .withSelfRel()
                .withTitle("Detalles de este producto");
    }

    private List<Link> crearEnlacesAdicionalesProducto(Producto p) {
        return Arrays.asList(
                crearEnlaceProducto(p), linkTo(methodOn(this.getClass()).obtenerProductos()).withRel("productos").withTitle("Todos los productos")
                , linkTo(methodOn(ControladorProveedoresProducto.class).obtenerProveedoresProducto(p.getId())).withRel("proveedores").withTitle("Lista de proveedores")
        );
    }

    private EntityModel<Producto> obtenerRespuestaProducto(Producto p) {
        return EntityModel.of(p).add(crearEnlacesAdicionalesProducto(p));
    }

}
package mis.cursos.springproductos_BACK01.controlador;

import mis.cursos.springproductos_BACK01.entitys.Product;
import mis.cursos.springproductos_BACK01.modelo.Producto;
import mis.cursos.springproductos_BACK01.modelo.Proveedor;
import mis.cursos.springproductos_BACK01.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping("/almacen/v1/productos/{idProducto}/proveedores")
public class ControladorProveedoresProducto {

    @Autowired
    ServicioGenerico<Producto> servicioProducto;

    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor;

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedoresProducto(@PathVariable long idProducto) {
        List<Integer> idsProveedores = this.servicioProducto.obtenerPorId(idProducto).getIdsProveedores();
        List<Proveedor> pp = idsProveedores.stream().map(idProveedor -> this.servicioProveedor.obtenerPorId(idProveedor)).collect(Collectors.toList());

        return CollectionModel.of(pp.stream().map(p -> EntityModel.of(p).add(linkTo(methodOn(ControladorProveedor.class).obtenerProveedorPorId(p.getId())).withSelfRel())).collect(Collectors.toList()))
                .add(linkTo(methodOn(ControladorProducto.class).obtenerProductoPorId(idProducto)).withRel("producto").withTitle("Productos"))
                .add(linkTo(methodOn(this.getClass()).obtenerProveedoresProducto(idProducto)).withSelfRel().withTitle("Proveedores"));
    }

    @GetMapping("/{indiceProveedor}")
    public Proveedor obtenerProveedoresProducto(@PathVariable long idProducto,
                                                @PathVariable int indiceProveedor) {
        final long idProveedor = this.servicioProducto
                .obtenerPorId(idProducto)
                .getIdsProveedores().get(indiceProveedor);
        return this.servicioProveedor.obtenerPorId(idProveedor);
    }

    static class ProveedorProducto {
        public int idProveedor;
    }

    @PostMapping
    public void agregarProveedorProducto(@PathVariable long idProducto,
                                         @RequestBody ProveedorProducto prov) {
        System.out.println("Producto: "+idProducto +" Proveedor "+ prov.idProveedor);
        try {
            this.servicioProveedor.obtenerPorId(prov.idProveedor);
            this.servicioProducto.obtenerPorId(idProducto)
                    .getIdsProveedores().add(prov.idProveedor);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{indiceProveedorProducto}")
    public void borrarProveedorProducto(@PathVariable long idProducto,
                                        @PathVariable int indiceProveedorProducto) {
        try {
            final List<Integer> listaIds = this.servicioProducto
                    .obtenerPorId(idProducto)
                    .getIdsProveedores();
            try {
                listaIds.remove(indiceProveedorProducto);
            } catch (Exception e) {
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
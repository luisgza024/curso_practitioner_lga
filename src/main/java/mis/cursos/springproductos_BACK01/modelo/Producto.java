package mis.cursos.springproductos_BACK01.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Producto {
    long id;
    String nombre;
    double precio;
    double cantidad;
    final List<Integer> idsProveedores = new ArrayList<>();
    final List<Usuario> usuarios = new ArrayList<>();

    public Producto() {
    }

    public Producto(long id, String nombre, double precio, double cantidad) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public List<Integer> getIdsProveedores() {
        return idsProveedores;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Producto))
            return false;
        Producto p = (Producto) o;
        return p.id == id;
    }

    public List<Usuario> getUsers() {
        return usuarios;
    }
}